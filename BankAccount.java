import java.util.concurrent.locks.*;
public class BankAccount
{
    private double balance;
    private ReentrantLock balanceChangeLock;
    private Condition sufficientFundsCondition;

    public BankAccount()
    {
	ReentrantLock balanceChangeLock = new ReentrantLock();
        balance = 0;
    }

    public void deposit(double amount)
    {
	balanceChangeLock.lock();

	try{
        System.out.print("Depositing " + amount);
        double newBalance = balance + amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;

	sufficientFundsCondition.signalAll();
	}catch (InterruptedException a) {}
	finally{
	balanceChangeLock.unlock();
	}
    }

    public void withdraw(double amount)
    {
	balanceChangeLock.lock();

	try{
	    while(balance < amount)
		sufficientFundsCondition.await();
	    
        System.out.print("Withdrawing " + amount);
        double newBalance = balance - amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;

	}catch (InterruptedException a){}
	    finally{
	    balanceChangeLock.unlock();
	}
    }

    public double getBalance()
    {
        return balance;
    }
}
